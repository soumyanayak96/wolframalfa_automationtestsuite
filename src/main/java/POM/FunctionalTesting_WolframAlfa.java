package POM;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Generics.BasePage;
import Generics.ExcelLibrary;


public class FunctionalTesting_WolframAlfa extends BasePage
{
	
	@FindBy(xpath="//span[contains(text(), 'This site can’t be reached')]")
	 private WebElement pageLoadError;
	
	@FindBy(xpath="//img[@alt='WolframAlpha computational knowledge AI']")
	 private WebElement wolframAlfaLink;

	@FindBy(xpath="//input[@placeholder='Enter what you want to calculate or know about']")
	 private WebElement seachTextField;
	   
	 @FindBy(xpath="//button[@type='submit']")
	 private WebElement submitButton;
	 
	 @FindBy(xpath="(//section[@tabindex='0'])[1]")
	 private WebElement inputPod;
	 
	 @FindBy(xpath="(//span[contains(text(),'Enlarge')])[1]")
	 private WebElement enlargeButton;
	 
	 @FindBy(xpath="(//span[contains(text(),'Data')])[1]")
	 private WebElement dataButton;
	 
	 @FindBy(xpath="(//span[contains(text(),'Customize')])[1]")
	 private WebElement customizedButton;
	 
	 @FindBy(xpath="(//span[contains(text(),'Plain Text')])[1]")
	 private WebElement plainTextButton;
	 
	 @FindBy(xpath="//span[contains(text(),'Wolfram|Alpha Copyable Plain Text')]")
	 private WebElement wolframAlphaCopyablePod;
	 
	 @FindBy(xpath="(//span[contains(text(),'Continue in computable notebook')])[1]")
	 private WebElement computableNotebookButton;
	 
	 @FindBy(xpath="//h2[contains(text(),'WOLFRAM CLOUD')]")
	 private WebElement wolframCloudPage;
	 
	 @FindBy(xpath="(//div[@class='SubLayoutBox-Outer FrameBox-Outer border-box-sizing'])[3]")
	 private WebElement runCodeButton1;
	 
	 @FindBy(xpath="(//div[@class='SubLayoutBox-Outer FrameBox-Outer border-box-sizing'])[4]")
	 private WebElement runCodeButton2;
	
	
	public FunctionalTesting_WolframAlfa(WebDriver driver)
	{
	   this.driver=driver;
	   PageFactory.initElements(driver, this);
	}

	 public void functionalTestMethod() throws IOException, InterruptedException
	   {
		 
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,20);
			 wait.until(ExpectedConditions.visibilityOf(wolframAlfaLink));
			 wolframAlfaLink.isDisplayed();
			 System.out.println("Application is opened.");
		 }
		catch(Throwable exp){
			pageLoadError.isDisplayed();
			System.out.println("Application is not opened.");
			System.out.println(exp);
		 }
		 
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,20);
			 wait.until(ExpectedConditions.visibilityOf(seachTextField));
			 seachTextField.isDisplayed();
			 System.out.println("Search textfield is displaying.");
		 }
		catch(Throwable exp){
			pageLoadError.isDisplayed();
			System.out.println("Search textfield is not displaying.");
			System.out.println(exp);	
		 }
		 
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,20);
			 wait.until(ExpectedConditions.visibilityOf(submitButton));
			 submitButton.isDisplayed();
			 System.out.println("Submit button is displaying.");
		 }
		 catch(Exception Exp) {
			 System.out.println("Submit button is not displaying.");
			 System.out.println(Exp);
		 }
		  
		 try {
			 String input=ExcelLibrary.getCellValue("C:/Users/soumya/workspace/WolframAlfa_AutomationTestsuite/ParamerFile_WolframAlfa/Parameters.xlsx", "Sheet1", 1, 1);
			 seachTextField.sendKeys(input);
			 WebDriverWait wait= new WebDriverWait(driver,20);
			 wait.until(ExpectedConditions.visibilityOf(submitButton));
			 submitButton.click();
			 System.out.println("Desired result is displaying.");
		 }
		 catch(Exception Exp) {
			 System.out.println("Desired result is not displaying.");
			 System.out.println(Exp);
		 }
		 
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,30);
			 wait.until(ExpectedConditions.visibilityOf(inputPod));
			 inputPod.isDisplayed(); 
			 Actions act=new Actions(driver);
			 act.moveToElement(inputPod).click(inputPod).perform();
			 System.out.println("Input pod is displaying.");
		 }
		 catch(Exception Exp) {
			 System.out.println("Input pod is not displaying.");
			 System.out.println(Exp);
		 }
		 
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,20);
			 wait.until(ExpectedConditions.visibilityOf(enlargeButton));
			 enlargeButton.isDisplayed(); 
			 System.out.println("Enlarge button is displaying.");
		 }
		 catch(Exception Exp) {
			 System.out.println("Enlarge button is not displaying.");
			 System.out.println(Exp);
		 }
		 
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,20);
			 wait.until(ExpectedConditions.visibilityOf(dataButton));
			 dataButton.isDisplayed(); 
			 System.out.println("Data button is displaying.");
		 }
		 catch(Exception Exp) {
			 System.out.println("Data button is not displaying.");
			 System.out.println(Exp);
		 }
		 
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,20);
			 wait.until(ExpectedConditions.visibilityOf(customizedButton));
			 customizedButton.isDisplayed(); 
			 System.out.println("Customized button is displaying.");
		 }
		 catch(Exception Exp) {
			 System.out.println("Customized button is not displaying.");
			 System.out.println(Exp);
		 }
		 
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,20);
			 wait.until(ExpectedConditions.visibilityOf(plainTextButton));
			 plainTextButton.isDisplayed(); 
			 System.out.println("Plain Text button is displaying.");
		 }
		 catch(Exception Exp) {
			 System.out.println("Plain Text button is not displaying.");
			 System.out.println(Exp);
		 }
		 
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,20);
			 wait.until(ExpectedConditions.visibilityOf(plainTextButton));
			 plainTextButton.click();
			 wait.until(ExpectedConditions.visibilityOf(wolframAlphaCopyablePod));
			 wolframAlphaCopyablePod.isDisplayed(); 
			 System.out.println("Wolfram Alpha Copyable Plain Text pod is displaying.");
		 }
		 catch(Exception Exp) {
			 System.out.println("Wolfram Alpha Copyable Plain Text pod is not displaying.");
			 System.out.println(Exp);
		 }
		 
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,20);
			 wait.until(ExpectedConditions.visibilityOf(computableNotebookButton));
			 computableNotebookButton.isDisplayed(); 
			 System.out.println("Computable Notebook button is displaying.");
		 }
		 catch(Exception Exp) {
			 System.out.println("Computable Notebook button is not displaying.");
			 System.out.println(Exp);
		 }
		 
		 try {
			 ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", computableNotebookButton);
			 WebDriverWait wait= new WebDriverWait(driver,30);
			 wait.until(ExpectedConditions.visibilityOf(computableNotebookButton));
			 computableNotebookButton.click();
			 wait.until(ExpectedConditions.visibilityOf(wolframCloudPage));
			 String actualText=wolframCloudPage.getText(); 
			 String expectedText=ExcelLibrary.getCellValue("C:/Users/soumya/workspace/WolframAlfa_AutomationTestsuite/ParamerFile_WolframAlfa/Parameters.xlsx", "Sheet1", 3, 1);
			 if(actualText.contains(expectedText))
			 System.out.println("User is redirected to Wolfram Cloud page.");
		 }
		 catch(Exception Exp) {
			 System.out.println("User is not redirected to Wolfram Cloud page.");
			 System.out.println(Exp);
		 }
		 
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,60);
			 wait.until(ExpectedConditions.visibilityOf(runCodeButton1));
			 runCodeButton1.isDisplayed();
			 System.out.println("Run Code button is displaying");
		 }
		 catch(Exception Exp) {
			 System.out.println("Run Code button is not displaying");
			 System.out.println(Exp);
		 }  
		 
		//Check Run code button 
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,60);
			 wait.until(ExpectedConditions.visibilityOf(runCodeButton2));
			 runCodeButton2.isDisplayed();
			 System.out.println("Run Code button is displaying.");
		 }
		 catch(Exception Exp) {
			 System.out.println("Run Code button is not displaying.");
			 System.out.println(Exp);
		 }  
		 
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,60);
			 wait.until(ExpectedConditions.visibilityOf(runCodeButton1));
			 runCodeButton1.click();
			 System.out.println("Entered number is evaluated and returned the expected value.");
		 }
		 catch(Exception Exp) {
			 System.out.println("Entered number is not evaluated and not returned the expected value.");
			 System.out.println(Exp);
		 }  
		 
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,60);
			 wait.until(ExpectedConditions.visibilityOf(runCodeButton2));
			 runCodeButton2.click();
			 System.out.println("Entered number is evaluated and returned the expected value.");
		 }
		 catch(Exception Exp) {
			 System.out.println("Entered number is not evaluated and not returned the expected value.");
			 System.out.println(Exp);
		 }
		 
		 
		 
		 
		 
		 
	   }
}
