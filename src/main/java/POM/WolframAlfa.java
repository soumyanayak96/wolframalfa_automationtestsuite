package POM;

import java.io.IOException;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Generics.BasePage;
import Generics.ExcelLibrary;


public class WolframAlfa extends BasePage
 {
	
	 @FindBy(xpath="//input[@placeholder='Enter what you want to calculate or know about']")
	 private WebElement seachTextField;
	   
	 @FindBy(xpath="//button[@type='submit']")
	 private WebElement submitButton;
	   
	 @FindBy(xpath="(//section[@tabindex='0'])[1]")
	 private WebElement inputArea;
	 
	 @FindBy(xpath="(//span[contains(text(),'Plain Text')])[1]")
	 private WebElement plainTextButton;
	 
	 @FindBy(xpath="(//span[contains(text(),'Continue in computable notebook')])[1]")
	 private WebElement computableNotebookButton;
	 
	 @FindBy(xpath="(//div[@class='SubLayoutBox-Outer FrameBox-Outer border-box-sizing'])[3]")
	 private WebElement runCodeButton1;
	 
	 @FindBy(xpath="(//div[@class='SubLayoutBox-Outer FrameBox-Outer border-box-sizing'])[4]")
	 private WebElement runCodeButton2;


	public WolframAlfa(WebDriver driver)
	{
	   this.driver=driver;
	   PageFactory.initElements(driver, this);
	}

	 public void customMethod() throws IOException, InterruptedException
	   {
		//Input in Search textfield
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,20);
			 wait.until(ExpectedConditions.visibilityOf(seachTextField));
			 String input=ExcelLibrary.getCellValue("C:/Users/soumya/workspace/WolframAlfa_AutomationTestsuite/ParamerFile_WolframAlfa/Parameters.xlsx", "Sheet1", 1, 1);
			 seachTextField.sendKeys(input); 
		 }
		 catch(Exception Exp) {
			 System.out.println(Exp);
		 }
		 
		//Click submit button 
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,20);
			 wait.until(ExpectedConditions.visibilityOf(submitButton));
			 submitButton.click(); 
		 }
		 catch(Exception Exp) {
			 System.out.println(Exp);
		 }
		 	
		 //Mouse Hover
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,20);
			 wait.until(ExpectedConditions.visibilityOf(inputArea));
			 Actions act=new Actions(driver);
			 act.moveToElement(inputArea).click(inputArea).perform();
		 }
		 catch(Exception Exp) {
			 System.out.println(Exp);
		 } 
		 
		 //Click plain text button
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,20);
			 wait.until(ExpectedConditions.visibilityOf(plainTextButton));
			 plainTextButton.click();
		 }
		 catch(Exception Exp) {
			 System.out.println(Exp);
		 } 
		 
		 //Click computable button
		 try {
			 ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", computableNotebookButton);
			 WebDriverWait wait= new WebDriverWait(driver,20);
			 wait.until(ExpectedConditions.visibilityOf(computableNotebookButton));
			 computableNotebookButton.click(); 
		 }
		 catch(Exception Exp) {
			 System.out.println(Exp);
		 }  
		 
		 //Check Run code button 
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,60);
			 wait.until(ExpectedConditions.visibilityOf(runCodeButton1));
			 runCodeButton1.isDisplayed();
		 }
		 catch(Exception Exp) {
			 System.out.println(Exp);
		 }  
		 
		//Check Run code button 
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,60);
			 wait.until(ExpectedConditions.visibilityOf(runCodeButton2));
			 runCodeButton2.isDisplayed();
		 }
		 catch(Exception Exp) {
			 System.out.println(Exp);
		 }  
		 
		//Click Run code button
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,40);
			 wait.until(ExpectedConditions.visibilityOf(runCodeButton1));
			 runCodeButton1.click();
		 }
		 catch(Exception Exp) {
			 System.out.println(Exp);
		 }  
		 
		//Click Run code button
		 try {
			 WebDriverWait wait= new WebDriverWait(driver,40);
			 wait.until(ExpectedConditions.visibilityOf(runCodeButton2));
			 runCodeButton2.click();
		 }
		 catch(Exception Exp) {
			 System.out.println(Exp);
		 } 
		 
		 
		}	 
 }