package Generics;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest 
{
  public WebDriver driver;
  
  @BeforeMethod
  public void openApplication()
  {
	  System.setProperty("webdriver.chrome.driver", "C:/Applications/chromedriver.exe");
	  driver=new ChromeDriver();
	//driver=new FirefoxDriver();
	  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  driver.manage().window().maximize();
	  driver.get("https://www.wolframalpha.com/");
	 
  }
  
  @AfterMethod
  public void closeApplication()
  {
	  driver.close();	  
  }	
}

