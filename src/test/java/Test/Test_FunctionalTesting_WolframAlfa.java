package Test;

import java.io.IOException;

import org.testng.annotations.Test;

import Generics.BaseTest;
import POM.FunctionalTesting_WolframAlfa;
import POM.WolframAlfa;

public class Test_FunctionalTesting_WolframAlfa extends BaseTest

{

	@Test
	 public void WolframAlfaFunctionalTest() throws IOException, InterruptedException
	 {
		FunctionalTesting_WolframAlfa test=new FunctionalTesting_WolframAlfa(driver);
		 test.functionalTestMethod();
		 
	 }
}
